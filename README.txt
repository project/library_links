CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Example
 * Theming and Output
 * Maintainers


INTRODUCTION
------------

Library links module provides a standard custom field which act as link but the actual link is referenced from a taxonomy vocabulary.
It also contains a checkbox , where we can select whether we want to reference a link or add a link in input box.
It also supports additional link text title. 


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal
   module.
   Visit: https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

 * you should also download the Library Links into the module/contrib directory of your Drupal
  installation.


CONFIGURATION
-------------

  * Configure Library Links in Administration » Configuration »
   Library Links Settings
  * This configuration is required for showing options of terms on the library links field.

  Here choose the Reference type for the vocabulary to use for reference.


EXAMPLE
-------
If you were to create a field named 'My New Link', the default display of the
link would be:
<a href="{{ link }}" title="{{ link_title }}">{{ link_title }}</a>
where value between {{ }} characters would be customized
based on the checkbox , If checkbox is checked , then Link is rendered from Entity Referenced field value.
and if unchecked , then link is rendered from Link field value.



MAINTAINERS
-----------

Current maintainers:
 * Gal Magrisso - https://www.drupal.org/u/gal-magrisso
 * Peleg Rabinowitz (pelegr) - https://www.drupal.org/u/pelegr

