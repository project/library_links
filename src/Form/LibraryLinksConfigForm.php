<?php

namespace Drupal\links_library\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Library Links configuration form.
 */
class LibraryLinksConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'links_library.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'library_links_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('links_library.settings');

    $vocabularies = $this->entityTypeManager
      ->getStorage('taxonomy_vocabulary')
      ->loadMultiple();

    $options = [];

    $options[''] = $this->t('- None -');

    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->id()] = $vocabulary->label();
    }

    $vocabulary_add_url = Url::fromRoute('entity.taxonomy_vocabulary.add_form')->toString();
    $form['library_taxonomy'] = [
      '#type' => 'select',
      '#title' => $this->t('Taxonomy'),
      '#description' => $this->t('Select taxonomy for directory structure or  <a href=":create_vocabulary_url">create a new one</a>.', [
        ':create_vocabulary_url' => $vocabulary_add_url,
      ]),
      '#options' => $options,
      '#default_value' => $config->get('library_taxonomy'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('links_library.settings')
      ->set('library_taxonomy', $form_state->getValue('library_taxonomy'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
