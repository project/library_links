<?php

namespace Drupal\links_library\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\links_library\Plugin\Field\FieldType\LinksLibrary;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'link_library_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "link_library_formatter",
 *   label = @Translation("Link Library formatter"),
 *   field_types = {
 *     "links_library"
 *   }
 * )
 */
class LinkLibraryFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    if (!empty($items)) {
      $elements = [
        '#type' => 'container',
      ];
      foreach ($items as $delta => $item) {
        $elements[$delta] = $this->viewElement($item, $langcode);
      }
    }
    return $elements;
  }

  /**
   * Helper function for view one link.
   */
  protected function viewElement(LinksLibrary $item, $langcode) {
    $link = NULL;
    $link_title = '';
    if ($item->link_from_library == 1) {
      if (!empty($item->target_id)) {
        $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($item->target_id);
        if ($term) {
          $link = $term->field_website_link->first()->getUrl();
          $link_title = $term->field_website_link->first()->title;
        }
      }
    }
    else {
      $link = $item->url;
      $link_title = $item->title;
    }
    $element = [
      '#theme' => 'link_library',
      '#link' => $link,
      '#link_title' => $link_title,
    ];
    return $element;
  }

}
