<?php

namespace Drupal\links_library\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'links_library' field type.
 *
 * @FieldType (
 *   id = "links_library",
 *   label = @Translation("Links Library"),
 *   description = @Translation("Create Links Library field type"),
 *   default_widget = "link_library_widget",
 *   default_formatter = "link_library_formatter",
 * )
 */
class LinksLibrary extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'target_id' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ],
        'link_from_library' => [
          'type' => 'int',
          'description' => 'Links refer to library.',
          'size' => 'tiny',
        ],
        'title' => [
          'description' => 'Title of the Link',
          'type' => 'varchar',
          'length' => '255',
        ],
        'url' => [
          'description' => 'The URI of the link.',
          'type' => 'varchar',
          'length' => 2048,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {

    $value1 = $this->get('target_id')->getValue();
    $value2 = $this->get('link_from_library')->getValue();
    $value3 = $this->get('title')->getValue();
    $value4 = $this->get('url')->getValue();
    return empty($value1) && empty($value2) && empty($value3) && empty($value4);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['target_id'] = DataDefinition::create('integer')
      ->setLabel(t('Referenced Library'));

    $properties['link_from_library'] = DataDefinition::create('boolean')
      ->setLabel(t('Link From Library'));

    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Title Value'));

    $properties['url'] = DataDefinition::create('string')
      ->setLabel(t('URI'));

    return $properties;
  }

}
